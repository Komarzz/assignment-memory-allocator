//
// Created by Sasha Komarov on 23.01.2023.
//

#ifndef ASSIGNMENT_MEMORY_ALLOCATOR_TEST_H
#define ASSIGNMENT_MEMORY_ALLOCATOR_TEST_H

#include "mem.h"
#include "mem_internals.h"

void test1_allocate();
void test2_allocate2_free1();
void test3_allocate3_free2();
void test4_allocate_new_region();
void test5_cant_allocate_new_region();

#endif //ASSIGNMENT_MEMORY_ALLOCATOR_TEST_H
