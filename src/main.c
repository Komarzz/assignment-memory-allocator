//
// Created by Sasha Komarov on 23.01.2023.
//

#include "test.h"

int main(void){
    heap_init(10000);
    printf("Hello");
    test1_allocate();
    test2_allocate2_free1();
    test3_allocate3_free2();
    test4_allocate_new_region();
    test5_cant_allocate_new_region();
}
