//
// Created by Sasha Komarov on 23.01.2023.
//


#include "test.h"


void test1_allocate(){
    printf("Test 1 start");
    debug_heap(stdout, HEAP_START);
    void *block = _malloc(10);
    debug_heap(stdout, HEAP_START);
    if(!block){
        fprintf(stderr, "Failed\n");
    }else{
        fprintf(stderr, "Success\n");
    }
    _free(block);
}

void test2_allocate2_free1(){
    printf("Test 2 start");
    void *firstBlock = _malloc(20);
    void *secondBlock = _malloc(40);
    debug_heap(stdout, HEAP_START);
    if (!firstBlock || !secondBlock) {
        fprintf(stderr, "Failed\n");
    }else{
        fprintf(stderr, "Success\n");
    }
    _free(firstBlock);
    debug_heap(stdout, HEAP_START);
    _free(secondBlock);
}

void test3_allocate3_free2(){
    printf("Test 3 start");
    void *firstBlock = _malloc(10);
    void *secondBlock = _malloc(20);
    void *thirdBlock = _malloc(40);
    if (!firstBlock || !secondBlock || !thirdBlock) {
        fprintf(stderr, "Failed\n");
    }else{
        fprintf(stderr, "Success\n");
    }

    debug_heap(stdout, HEAP_START);
    _free(firstBlock);
    _free(secondBlock);
    debug_heap(stdout, HEAP_START);


    _free(thirdBlock);
    debug_heap(stdout, HEAP_START);
}

void test4_allocate_new_region(){
    printf("Test 4 start");
    void *firstRegion = _malloc(10000);
    void *secondRegion = _malloc(10000);
    if (!firstRegion || !secondRegion) {
        fprintf(stderr, "Failed\n");
    }else{
        fprintf(stderr, "Success\n");
    }
    debug_heap(stdout, HEAP_START);
    _free(firstRegion);
    _free(secondRegion);
    debug_heap(stdout, HEAP_START);
}

void test5_cant_allocate_new_region(){
    printf("Test 5 start");
    void *firstRegion = _malloc(10000);
    void *secondRegion = _malloc(100000);
    if (!firstRegion || !secondRegion) {
        fprintf(stderr, "Failed\n");
    }else{
        fprintf(stderr, "Success\n");
    }
    debug_heap(stdout, HEAP_START);
    _free(firstRegion);
    _free(secondRegion);
    debug_heap(stdout, HEAP_START);
}
