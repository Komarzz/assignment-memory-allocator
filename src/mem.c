#include <stdarg.h>
#define _DEFAULT_SOURCE
#include <unistd.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <assert.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header* b, const char* fmt, ... );
void debug(const char* fmt, ... );

extern inline block_size size_from_capacity( block_capacity cap );
extern inline block_capacity capacity_from_size( block_size sz );

static bool            block_is_big_enough( size_t query, struct block_header* block ) { return block->capacity.bytes >= query; }
static size_t          pages_count   ( size_t mem )                      { return mem / getpagesize() + ((mem % getpagesize()) > 0); }
static size_t          round_pages   ( size_t mem )                      { return getpagesize() * pages_count( mem ) ; }

static void block_init( void* restrict addr, block_size block_sz, void* restrict next ) {
  *((struct block_header*)addr) = (struct block_header) {
    .next = next,
    .capacity = capacity_from_size(block_sz),
    .is_free = true
  };
}

static size_t region_actual_size( size_t query ) { return size_max( round_pages( query ), REGION_MIN_SIZE ); }

#define BLOCK_MIN_CAPACITY 24



extern inline bool region_is_invalid( const struct region* r );

static void* map_pages(void const* addr, size_t length, int additional_flags) {
  return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , 0, 0 );
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region  ( void const * addr, size_t query ) {

    if(!addr){
        return REGION_INVALID;
    }
    if(BLOCK_MIN_CAPACITY > query){
        query = BLOCK_MIN_CAPACITY;
    }

    const size_t regionSize = region_actual_size(query + offsetof(struct block_header, contents));

    struct region newRegion = {0};
    newRegion.size = regionSize;
    newRegion.extends = true;

    void *mmap_addr = map_pages(addr, regionSize, MAP_FIXED);

    if (mmap_addr == MAP_FAILED) {
        mmap_addr = map_pages(addr, regionSize, 0);

        if (mmap_addr == MAP_FAILED)
            return REGION_INVALID;

        newRegion.extends = false;
    }

    newRegion.addr = mmap_addr;
    block_size blockSize;
    blockSize.bytes = regionSize;
    block_init(mmap_addr, blockSize, NULL);

    return newRegion;

}

static void* block_after( struct block_header const* block )         ;

void* heap_init( size_t initial ) {
  const struct region region = alloc_region( HEAP_START, initial );
  if ( region_is_invalid(&region) ) return NULL;

  return region.addr;
}

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable( struct block_header* restrict block, size_t query) {
  return block-> is_free && query + offsetof( struct block_header, contents ) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big( struct block_header* block, size_t query ) {

    if(!block){
        return false;
    }
    if (BLOCK_MIN_CAPACITY > query) {
        query = BLOCK_MIN_CAPACITY;
    }

    if (block_splittable(block, query)) {
        struct block_header* splBlock = (struct block_header*) (block->contents + query);
        block_size blockSize = {.bytes = block->capacity.bytes - query};
        block_init(splBlock, blockSize, block->next);
        block->capacity.bytes = query;
        block->next = splBlock;
        return true;
    }
    return false;
}


/*  --- Слияние соседних свободных блоков --- */

static void* block_after( struct block_header const* block )              {
  return  (void*) (block->contents + block->capacity.bytes);
}
static bool blocks_continuous (
                               struct block_header const* fst,
                               struct block_header const* snd ) {
  return (void*)snd == block_after(fst);
}

static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {
  return fst->is_free && snd->is_free && blocks_continuous( fst, snd ) ;
}

static bool try_merge_with_next( struct block_header* block ) {

    if ((block->next && mergeable(block, block->next)) || block != NULL) {
        block->capacity.bytes += size_from_capacity(block->next->capacity).bytes;
        block->next = (block->next)->next;
        return true;
    }
    return false;
}


/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
  enum {BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED} type;
  struct block_header* block;
};


static struct block_search_result find_good_or_last  ( struct block_header* restrict block, size_t sz ) {
    static struct block_search_result blockSearchResult;
    if (block == NULL) {
        blockSearchResult.block = NULL;
        blockSearchResult.type = BSR_CORRUPTED;
        return blockSearchResult;
    }
    struct block_header *currentBlock = block;
    struct block_header *nextBlock = block;
    while (currentBlock) {

        while (try_merge_with_next(currentBlock));

        if (block_is_big_enough(sz, currentBlock) && currentBlock->is_free) {
            blockSearchResult.type = BSR_FOUND_GOOD_BLOCK;
            blockSearchResult.block = currentBlock;
            return blockSearchResult;
        }
        nextBlock = currentBlock;
        currentBlock = currentBlock->next;
    }
    blockSearchResult.type = BSR_REACHED_END_NOT_FOUND;
    blockSearchResult.block = nextBlock;
    return blockSearchResult;

}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing ( size_t query, struct block_header* block ) {

    if(block != NULL){
        return (struct block_search_result) {.type = BSR_CORRUPTED};
    }
    if(BLOCK_MIN_CAPACITY > query){
        query = BLOCK_MIN_CAPACITY;
    }

    struct block_search_result blockSearchResult = find_good_or_last(block, query);
    if (blockSearchResult.type == BSR_FOUND_GOOD_BLOCK) {
        split_if_too_big(blockSearchResult.block, query);
        blockSearchResult.block->is_free = false;
    }
    return blockSearchResult;

}



static struct block_header* grow_heap( struct block_header* restrict last, size_t query ) {

    if(BLOCK_MIN_CAPACITY > query){
        query = BLOCK_MIN_CAPACITY;
    }
    if(!last){
        return NULL;
    }

    struct region newRegion = alloc_region(block_after(last), query);
    if(!region_is_invalid(&newRegion)){
        return newRegion.addr;
    }
    if (try_merge_with_next(last) || last != NULL) {
        return last;
    }
    last->next = newRegion.addr;
    last->capacity.bytes += newRegion.size;
    return newRegion.addr;

}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header* memalloc( size_t query, struct block_header* heap_start) {

    if (BLOCK_MIN_CAPACITY > query) {
        query = BLOCK_MIN_CAPACITY;
    }
    if (heap_start) {

        struct block_search_result blockSearchResult = try_memalloc_existing(query, heap_start);

        if (blockSearchResult.type == BSR_REACHED_END_NOT_FOUND) {

            if (grow_heap(blockSearchResult.block, query)) {
                blockSearchResult = try_memalloc_existing(query, heap_start);
                return blockSearchResult.block;
            }
            return blockSearchResult.block;
        }
    }
    return NULL;

}

void* _malloc( size_t query ) {
  struct block_header* const addr = memalloc( query, (struct block_header*) HEAP_START );
  if (addr) return addr->contents;
  else return NULL;
}

static struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void _free( void* mem ) {
  if (!mem) return ;
  struct block_header* header = block_get_header( mem );
  header->is_free = true;
    while (try_merge_with_next(header));
}
